package com.android.kelolakontak.items;

public class ItemsDataKontak {
    String Fn;
    String Ln;
    String pictureThum;
    String pictureLarge;
    String dob;
    String gender;
    String phone;
    String cell;
    String locStreetNum;
    String locStreetName;
    String locCity;
    String locState;
    String locPoscode;
    String locLat;
    String locLong;



    String email;
    public ItemsDataKontak(String fn, String ln, String pictureThum, String pictureLarge, String dob, String gender, String phone, String cell, String locStreetNum, String locStreetName, String locCity, String locState, String locPoscode, String locLat, String locLong,String email) {
        Fn = fn;
        Ln = ln;
        this.pictureThum = pictureThum;
        this.pictureLarge = pictureLarge;
        this.dob = dob;
        this.gender = gender;
        this.phone = phone;
        this.cell = cell;
        this.locStreetNum = locStreetNum;
        this.locStreetName = locStreetName;
        this.locCity = locCity;
        this.locState = locState;
        this.locPoscode = locPoscode;
        this.locLat = locLat;
        this.locLong = locLong;
        this.email = email;
    }
    public String getEmail() {
        return email;
    }
    public String getFn() {
        return Fn;
    }

    public String getLn() {
        return Ln;
    }

    public String getPictureThum() {
        return pictureThum;
    }

    public String getPictureLarge() {
        return pictureLarge;
    }

    public String getDob() {
        return dob;
    }

    public String getGender() {
        return gender;
    }

    public String getPhone() {
        return phone;
    }

    public String getCell() {
        return cell;
    }

    public String getLocStreetNum() {
        return locStreetNum;
    }

    public String getLocStreetName() {
        return locStreetName;
    }

    public String getLocCity() {
        return locCity;
    }

    public String getLocState() {
        return locState;
    }

    public String getLocPoscode() {
        return locPoscode;
    }

    public String getLocLat() {
        return locLat;
    }

    public String getLocLong() {
        return locLong;
    }


}
