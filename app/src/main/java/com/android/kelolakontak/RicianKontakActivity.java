package com.android.kelolakontak;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RicianKontakActivity extends AppCompatActivity {
    LinearLayout linearClose;
    ImageView imageBesar;
    TextView name, dobText,kordinatText,tempatText,SelulerText,teleponText,surelText,genderText;
Button buttDell;
String index;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rician_kontak);

        linearClose = findViewById(R.id.linearClose);
        imageBesar = findViewById(R.id.imageBesar);
        name = findViewById(R.id.name);
        dobText = findViewById(R.id.dobText);
        buttDell = findViewById(R.id.buttDell);
        kordinatText = findViewById(R.id.kordinatText);
        tempatText = findViewById(R.id.tempatText);
        SelulerText = findViewById(R.id.SelulerText);
        teleponText = findViewById(R.id.teleponText);
        surelText = findViewById(R.id.surelText);
        genderText = findViewById(R.id.genderText);


        Intent ambil = getIntent();

        name.setText(ambil.getStringExtra("name"));
        dobText.setText(ambil.getStringExtra("dob"));
        kordinatText.setText(ambil.getStringExtra("kordinat"));
        tempatText.setText(ambil.getStringExtra("addre"));
        SelulerText.setText(ambil.getStringExtra("cell"));
        teleponText.setText(ambil.getStringExtra("telepon"));
        teleponText.setText(ambil.getStringExtra("telepon"));
        surelText.setText(ambil.getStringExtra("email"));
        String image = ambil.getStringExtra("foto");
         index = ambil.getStringExtra("index");

        String gender= ambil.getStringExtra("gender");
        if(gender.equals("female")){
            genderText.setText("Perempuan");
        }else{
            genderText.setText("Laki-laki");
        }

        Picasso.get().load(image).into(imageBesar);

        linearClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        buttDell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.listFinal.remove(Integer.valueOf(index));
                System.out.println("REMOVVVEEEEEE "+index);
                Intent pindah = new Intent(RicianKontakActivity.this, MainActivity.class);
                pindah.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(pindah);
            }
        });

    }
}