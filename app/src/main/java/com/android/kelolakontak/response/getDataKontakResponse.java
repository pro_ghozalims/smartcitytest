package com.android.kelolakontak.response;

public class getDataKontakResponse {
    private Results[] results;

    public Results[] getResults() {
        return results;
    }

    public void setResults(Results[] results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "ClassPojo [results = " + results + "]";
    }

    public class Results {
        private String gender;

        private String phone;

        private Dob dob;

        private Name name;

        private Location location;

        private String cell;

        private String email;

        private Picture picture;

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Dob getDob() {
            return dob;
        }

        public void setDob(Dob dob) {
            this.dob = dob;
        }

        public Name getName() {
            return name;
        }

        public void setName(Name name) {
            this.name = name;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public String getCell() {
            return cell;
        }

        public void setCell(String cell) {
            this.cell = cell;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public Picture getPicture() {
            return picture;
        }

        public void setPicture(Picture picture) {
            this.picture = picture;
        }

        @Override
        public String toString() {
            return "ClassPojo [gender = " + gender + ", phone = " + phone + ", dob = " + dob + ", name = " + name + ", location = " + location + ", cell = " + cell + ", email = " + email + ", picture = " + picture + "]";
        }
    }

    public class Location {
        private String country;

        private String city;

        private Street street;

        private Timezone timezone;

        private String postcode;

        private Coordinates coordinates;

        private String state;

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public Street getStreet() {
            return street;
        }

        public void setStreet(Street street) {
            this.street = street;
        }

        public Timezone getTimezone() {
            return timezone;
        }

        public void setTimezone(Timezone timezone) {
            this.timezone = timezone;
        }

        public String getPostcode() {
            return postcode;
        }

        public void setPostcode(String postcode) {
            this.postcode = postcode;
        }

        public Coordinates getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(Coordinates coordinates) {
            this.coordinates = coordinates;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        @Override
        public String toString() {
            return "ClassPojo [country = " + country + ", city = " + city + ", street = " + street + ", timezone = " + timezone + ", postcode = " + postcode + ", coordinates = " + coordinates + ", state = " + state + "]";
        }
    }

    public class Coordinates {
        private String latitude;

        private String longitude;

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        @Override
        public String toString() {
            return "ClassPojo [latitude = " + latitude + ", longitude = " + longitude + "]";
        }
    }

    public class Name {
        private String last;

        private String title;

        private String first;

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }

        @Override
        public String toString() {
            return "ClassPojo [last = " + last + ", title = " + title + ", first = " + first + "]";
        }
    }

    public class Timezone {
        private String offset;

        private String description;

        public String getOffset() {
            return offset;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return "ClassPojo [offset = " + offset + ", description = " + description + "]";
        }
    }

    public class Picture {
        private String thumbnail;

        private String large;

        private String medium;

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getLarge() {
            return large;
        }

        public void setLarge(String large) {
            this.large = large;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        @Override
        public String toString() {
            return "ClassPojo [thumbnail = " + thumbnail + ", large = " + large + ", medium = " + medium + "]";
        }
    }

    public class Street {
        private String number;

        private String name;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return "ClassPojo [number = " + number + ", name = " + name + "]";
        }
    }


    public class Dob {
        private String date;

        private String age;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "ClassPojo [date = " + date + ", age = " + age + "]";
        }
    }


}
