
package com.android.kelolakontak.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.kelolakontak.MainActivity;
import com.android.kelolakontak.R;
import com.android.kelolakontak.RicianKontakActivity;
import com.android.kelolakontak.items.ItemsDataKontak;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class AdapterKontak extends RecyclerView.Adapter<AdapterKontak.ItemViewHolder> {

    // private final List<String> mItems = new ArrayList<>();
    private List<ItemsDataKontak> mDataset;
    private List<ItemsDataKontak> contactList;

    Context context;

    public AdapterKontak(Context context2, ArrayList<ItemsDataKontak> myDataset) {

        mDataset = myDataset;
        contactList = myDataset;
        context = context2;
        //mItems.addAll(Arrays.asList(context.getResources().getStringArray(R.array.dummy_items)));
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_kontak, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }


    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        try {

            holder.listName.setText(mDataset.get(position).getFn()+" "+mDataset.get(position).getLn());
            System.out.println("LINKKK " + mDataset.get(position).getPictureThum());
            //   Picasso. (context).load(mDataset.get(position).getIcon()).into(holder.imageCar);
            Picasso.get().load(mDataset.get(position).getPictureThum()).into(holder.profile_image);

            holder.linearUtama.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent pindah=new Intent(context, RicianKontakActivity.class);
                        pindah.putExtra("index",String.valueOf(position));
                        pindah.putExtra("name",mDataset.get(position).getFn()+" "+mDataset.get(position).getLn());
                        pindah.putExtra("foto",mDataset.get(position).getPictureLarge());
                        pindah.putExtra("dob",mDataset.get(position).getDob());
                        pindah.putExtra("gender",mDataset.get(position).getGender());
                        pindah.putExtra("telepon",mDataset.get(position).getPhone());
                        pindah.putExtra("cell",mDataset.get(position).getCell());
                        pindah.putExtra("email",mDataset.get(position).getEmail());
                        pindah.putExtra("kordinat",mDataset.get(position).getLocLat()+","+mDataset.get(position).getLocLong());
                        pindah.putExtra("addre",mDataset.get(position).getLocStreetName()+" "+mDataset.get(position).getLocStreetNum()+" "+mDataset.get(position).getLocState());
                        pindah.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(pindah);


                    } catch (Exception er) {

                    }
                }
            });




        } catch (Exception er) {

        }


    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView listName;
        CircleImageView profile_image;
LinearLayout linearUtama;
        public ItemViewHolder(View itemView) {
            super(itemView);


            profile_image = itemView.findViewById(R.id.profile_image);
            listName =   itemView.findViewById(R.id.listName);
            linearUtama =   itemView.findViewById(R.id.linearUtama);


        }


    }


    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    System.out.println("Masuk ke Empty ");
                    mDataset = contactList;
                } else {
                    System.out.println(">>>> " + charString);
                    List<ItemsDataKontak> filteredList = new ArrayList<>();
                    for (ItemsDataKontak row : contactList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        System.out.println(">>> " + row.getFn().toLowerCase());
                        if (row.getFn().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        } else {
                            System.out.println("Ga ada yg cocok " + charString.toLowerCase());
                        }
                    }

                    mDataset = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mDataset;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mDataset = (List<ItemsDataKontak>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }





}
