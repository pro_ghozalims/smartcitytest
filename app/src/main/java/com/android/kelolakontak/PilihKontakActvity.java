package com.android.kelolakontak;

import android.os.Bundle;

import com.android.kelolakontak.adapter.AdapterListPilihKontak;
import com.android.kelolakontak.connection.ConnectService;
import com.android.kelolakontak.items.ItemsDataKontak;
import com.android.kelolakontak.response.getDataKontakResponse;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.widget.LinearLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PilihKontakActvity extends AppCompatActivity {
    LinearLayout linearClose;
    RecyclerView listKontak;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kontak_actvity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        linearClose = findViewById(R.id.linearClose);
        listKontak = findViewById(R.id.listKontak);

        getDataKontak();

    }

    private void getDataKontak() {

        System.out.println("Masuk ke data kontakk");


        ArrayList results = new ArrayList<ItemsDataKontak>();
        ConnectService gitHubService = ConnectService.retrofit.create(ConnectService.class);
        Call<getDataKontakResponse> call = gitHubService.getKontak();
        call.enqueue(new Callback<getDataKontakResponse>() {
            @Override
            public void onResponse(Call<getDataKontakResponse> call, Response<getDataKontakResponse> response) {
                try {
                    System.out.println("Responseee " + response.body().getResults().length);
                    System.out.println("Responseee " + response.body().getResults().toString());
                    int jumData = response.body().getResults().length;
                    for (int w = 0; w < jumData; w++) {
                        String Fn = response.body().getResults()[w].getName().getFirst();
                        String email = response.body().getResults()[w].getEmail();
                        String Ln = response.body().getResults()[w].getName().getLast();
                        String pictureThum = response.body().getResults()[w].getPicture().getThumbnail();
                        String pictureLarge = response.body().getResults()[w].getPicture().getLarge();
                        String dob = response.body().getResults()[w].getDob().getDate();
                        String gender = response.body().getResults()[w].getGender();
                        String phone = response.body().getResults()[w].getPhone();
                        String cell = response.body().getResults()[w].getCell();
                        String locStreetNum = response.body().getResults()[w].getLocation().getStreet().getNumber();
                        String locStreetName = response.body().getResults()[w].getLocation().getStreet().getName();
                        String locCity = response.body().getResults()[w].getLocation().getCity();
                        String locState = response.body().getResults()[w].getLocation().getState();
                        String locPoscode = response.body().getResults()[w].getLocation().getPostcode();
                        String locLat = response.body().getResults()[w].getLocation().getCoordinates().getLatitude();
                        String locLong = response.body().getResults()[w].getLocation().getCoordinates().getLongitude();

                        System.out.println("TESTTT "+Fn);
                        System.out.println("TESTTT "+pictureThum);



                        ItemsDataKontak item = new ItemsDataKontak(Fn, Ln, pictureThum, pictureLarge, dob, gender, phone, cell, locStreetNum, locStreetName, locCity, locState, locPoscode, locLat, locLong,email);
                        results.add(item);
                    }
                    final AdapterListPilihKontak adapter = new AdapterListPilihKontak(getApplicationContext(), results);
                    listKontak.setHasFixedSize(true);
                    listKontak.setAdapter(adapter);
                    listKontak.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

                } catch (Exception er) {
                    System.out.println("Ada error " + er.getMessage());
                }
            }

            @Override
            public void onFailure(Call<getDataKontakResponse> call, Throwable t) {
                System.out.println("Failure get Data " + t.getMessage());
            }
        });

    }
}