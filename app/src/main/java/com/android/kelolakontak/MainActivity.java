package com.android.kelolakontak;

import android.content.Intent;
import android.os.Bundle;

import com.android.kelolakontak.adapter.AdapterKontak;
import com.android.kelolakontak.adapter.AdapterListPilihKontak;
import com.android.kelolakontak.items.ItemsDataKontak;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    LinearLayout linearAdd;
    RecyclerView listKontak;
    public static ArrayList listFinal = new ArrayList<ItemsDataKontak>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        linearAdd = findViewById(R.id.linearAdd);
        listKontak = findViewById(R.id.listKontak);


        linearAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pindah = new Intent(MainActivity.this, PilihKontakActvity.class);

                startActivity(pindah);
            }
        });


        System.out.println("JUMLAH DATAAA list " + listFinal.size());

        if (!listFinal.isEmpty()) {
            System.out.println("Masuk ke dalam list tidak kosong");
            final AdapterKontak adapter = new AdapterKontak(getApplicationContext(), listFinal);
            listKontak.setHasFixedSize(true);
            listKontak.setAdapter(adapter);
            listKontak.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}