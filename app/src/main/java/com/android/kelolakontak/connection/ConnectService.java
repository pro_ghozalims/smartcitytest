package com.android.kelolakontak.connection;

import com.android.kelolakontak.response.getDataKontakResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ConnectService {


    @GET("api?results=5&exc=login,registered,id,nat&nat=us&noinfo")
    Call<getDataKontakResponse> getKontak(
    );

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://randomuser.me/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
